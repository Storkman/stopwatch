#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>

#define BUFS 1024

#define QUITAFTER 4000

long clksub(struct timespec a, struct timespec b);
void die(const char*, ...);
int notify(const char *pidp);
void printclk(struct timespec start, struct timespec wstart, struct timespec now, struct timespec wnow);
void sig1(int s);
void sig2(int s);
int swatch(struct timespec start, struct timespec wstart);

static int startstop = 0, reset = 0, quitnow = 0;
struct timespec stop, wstop;

int
main(int argc, char* argv[])
{
	char buf[BUFS];
	const char *home;
	int pidfd, err;
	FILE *f;
	struct timespec start, wstart;
	clock_gettime(CLOCK_MONOTONIC, &start);
	clock_gettime(CLOCK_REALTIME, &wstart);
	
	/* Try to create and write a PID file. If it exists, call notify() and quit. */
	home = getenv("HOME");
	if (!home)
		home = "/tmp";
	snprintf(buf, BUFS, "%s/.stopwatch-pid", home);
	pidfd = open(buf, O_CREAT|O_EXCL|O_WRONLY, 0600);
	if (pidfd < 0) {
		if (errno == EEXIST)
			return notify(buf);
		die("could not open PID file:");
	}
	
	f = fdopen(pidfd, "w");
	fprintf(f, "%d\n", getpid());
	fclose(f);
	
	err = swatch(start, wstart);
	
	unlink(buf);
	printf("\n");
	return err;
}

long
clksub(struct timespec a, struct timespec b)
{
	return (a.tv_sec - b.tv_sec)*1000 + (a.tv_nsec - b.tv_nsec)/1000000;
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	exit(1);
}

int
notify(const char *pidp)
{
	FILE *f;
	int pid, n;
	f = fopen(pidp, "r");
	if (!f)
		die("could not open PID file:");
	n = fscanf(f, "%d", &pid);
	if (n != 1)
		die("failed to parse PID\n");
	kill(pid, SIGUSR1);
	return 0;
}

void
printclk(struct timespec start, struct timespec wstart, struct timespec now, struct timespec wnow)
{
	long d;
	int h, m, s, t;
	struct tm *lt;
	
	d = clksub(now, start);
	t = d % 1000 / 100;
	d /= 1000;
	s = d % 60;
	d /= 60;
	m = d % 60;
	h = d / 60;
	
	if (h > 0)
		printf("[+%01d:%02d:%02d.%d", h, m, s, t);
	else
		printf("[+%01d:%02d.%d", m, s, t);
	
	lt = localtime(&wstart.tv_sec);
	printf(" %02d:%02d:%02d.%01ld]\n", lt->tm_hour, lt->tm_min, lt->tm_sec, wstart.tv_nsec/100000000);
	fflush(stdout);
}

void
sig1(int s)
{
	clock_gettime(CLOCK_MONOTONIC, &stop);
	clock_gettime(CLOCK_REALTIME, &wstop);
	startstop = 1;
}

void
sig2(int s)
{
	clock_gettime(CLOCK_MONOTONIC, &stop);
	clock_gettime(CLOCK_REALTIME, &wstop);
	reset = 1;
}

void
sigterm(int s)
{
	quitnow = 1;
}

#define MSEC(T) ((T).tv_sec*1000 + (T).tv_nsec/(1000*1000))

#define RUNNING 1
#define RESET 2

int
swatch(struct timespec start, struct timespec wstart)
{
	int status = RESET|RUNNING;
	int running = 1;
	struct timespec now, wnow, s;
	long msec, nstop = (MSEC(wstart) / 100) * 100;
	long resetms;
	
	signal(SIGUSR1, sig1);
	signal(SIGUSR2, sig2);
	signal(SIGTERM, sigterm);
	signal(SIGINT, sigterm);
	
	for (;;) {
		if (quitnow)
			return 0;
		if (reset) {
			reset = 0;
			if (status & RESET)
				return 0;
			status |= RESET;
			start = stop;
			wstart = wstop;
		}
		if (startstop) {
			startstop = 0;
			status ^= RUNNING;
			if (status & RESET) {
				start = stop;
				wstart = wstop;
			}
		}

		clock_gettime(CLOCK_MONOTONIC, &now);
		clock_gettime(CLOCK_REALTIME, &wnow);
		
		if (status == RESET) {
			printclk(start, wstart, start, wstart);
			msec = MSEC(now);
			resetms = MSEC(stop);
			if (msec - resetms > QUITAFTER)
				return 0;
			s.tv_sec = (QUITAFTER - msec + resetms) / 1000;
			s.tv_nsec = (QUITAFTER - msec + resetms) % 1000;
			s.tv_nsec *= 1000000;
			nanosleep(&s, NULL);
			continue;
		}
		
		if(status == 0) {
			printclk(start, wstart, stop, wstop);
			pause();
			continue;
		}
		
		if(status == RESET|RUNNING) {
			status = RUNNING;
		}
		
		msec = MSEC(wnow);
		if (msec < nstop) {
			s.tv_sec = 0;
			s.tv_nsec = (nstop - msec) * 1000000;
			nanosleep(&s, NULL);
			continue;
		}
		printclk(start, wstart, now, wnow);
		nstop = (MSEC(wnow) / 100) * 100 + 100;
	}
	return 0;
}
