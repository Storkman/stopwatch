# stopwatch.c
`stopwatch` prints the time at startup, plus elapsed time, every 0.1s on standard output.
Starting another instance sends `SIGUSR1` to the previous instance to pause/unpause the display.

`SIGUSR2` resets the counter to zero.

`stopwatch` exits after being stopped at zero for 4 seconds, or if it receives `SIGUSR2` when already stopped at zero.

## How do I use this?
Here's an example `xbindkeys` configuration using [astatus](https://bitbucket.org/Storkman/astatusbar/src/master/) to write to the `dwm` status bar:

----------------------------------------------------------

    "/usr/local/bin/stopwatch >$HOME/.astatusbar/swatch &"
    	m:0x40 + c:67
	
    "kill -USR2 $(cat $HOME/.stopwatch-pid)"
    	m:0x40 + c:68

----------------------------------------------------------

Press `Super+F1` to start/stop, `Super+F2` to reset.

## Files
`stopwatch` uses a file at `$HOME/.stopwatch-pid` to tell if another instance is already running.

## TODO
Log timestamps in a file for easy reference.
